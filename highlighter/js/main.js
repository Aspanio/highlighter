document.addEventListener('DOMContentLoaded', function() {

    const HighLighter = function (arr, main) {
        this.previewImage = arr ? arr : document.querySelectorAll('img:not(.main)');
        this.mainImage = main ? main : document.querySelector('.main');
       
        let tracker = function (arr, main) {
            for (let i = 0; i<arr.length; i++) {
                arr[i].addEventListener('mouseover', ()=> {
                    main.src = arr[i].src;
                    arr[i].style.boxShadow = '0px 0px 0px 2px grey'
                })
                arr[i].addEventListener('mouseout', ()=> {
                    arr[i].style.boxShadow = 'none'
                })
            }
        }
        tracker(this.previewImage, this.mainImage);
    }
    //Если кому-то надо, то он может передать свои селекторы, но 
    //я использую запасные прописаные в коде:)
    new HighLighter();
});
